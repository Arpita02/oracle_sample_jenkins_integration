CREATE OR REPLACE PACKAGE xxtest_har_pkg1 AS
/* merge conflict test -- tushar  harsha */
/* $XX_Header: xxtest_har_pkg.pks 11.5.10.2 2019/09/20 Harshavardhan_M $*/
/* $XX_Release: Standalone: RFG0007 $*/
/* CC_REF_NEW */
/* CC_REF_OLD */
--+=====================================================================+
--|                       R**** F***** G****                            |
--|                       P****** S****                                 |
--+=====================================================================+
--|                                                                     |
--|                                                                     |
--| TYPE          : Package Body                                        |
--| NAME          : xxtest_har_pkg                                      |
--| PURPOSE       : Created for test purpose                            |
--|                                                                     |
--| Modification History:                                               |
--| ---------------------                                               |
--|                                                                     |
--| Author           Date        Version         Description            |
--| -----------    -----------  ------------   ----------------         |
--| (Harsha)        15-AUG-2019  11.5.10.1      Initial revision        |
--| (Harsha)        20-SEP-2019  11.5.10.2      RFG0007 - Updated       |
--| (Harsha)        18-OCT-2019  11.5.10.7      RFG0013 - Updated         |
 --+====================================================================+
 
--+=======================================================================+
--| TYPE             : Procedure                                          |
--| NAME             : xxtest_har_output                                  |
--| INPUT Parameters : p_opco                                             |
--| OUTPUT Parameters: x_errbuff                                          |
--|                   ,x_retcode                                          |
--| PURPOSE          : Created for test purpose                           |
--| Modification History:                                                 |
--|----------------------                                                 |
--|                                                                       |
--| Author            Date        Version         Description             |
--| -----------     -----------  ------------     ----------------        |
--| (Harsha)        15-AUG-2019  11.5.10.1      Initial revision          |
--| (Harsha)        20-SEP-2019  11.5.10.2      RFG0007 - Updated         |
--| (Harsha)        24-SEP-2019  11.5.10.3      RFG0008 - Updated         |
--| (Harsha)        24-SEP-2019  11.5.10.4      RFG0009 - Updated         |
--| (Harsha)        24-SEP-2019  11.5.10.5      RFG0010 - Updated         |
--| (Harsha)        24-SEP-2019  11.5.10.6      RFG0011 - Updated         |
--| (Harsha)        24-SEP-2019  11.5.10.7      RFG0012 - Updated         |
--| (Harsha)        25-SEP-2019  11.5.10.7      RFG0013 - Updated         |
--| (Harsha)        21-OCT-2019  11.5.10.7      RFG0014 - Updated         |
--+=======================================================================+

  PROCEDURE xxtest_har_output(
             x_errbuff OUT NOCOPY VARCHAR2
            ,x_retcode OUT NOCOPY NUMBER
            ,p_opco hr_operating_units.organization_id%TYPE
            );

  -- Ver No: 11.5.10.2 is added for RFG0007
  -- Ver No: 11.5.10.2 is added for RFG0008
  -- Ver No: 11.5.10.3 is added for RFG00277
END xxtest_har_pkg;
/