CREATE OR REPLACE PACKAGE BODY xxtest_har_pkg AS
/* $XX_Header: xxtest_har_pkg.pkb 11.5.10.8 2019/09/23 Arpita_Kar $*/
/* $XX_Release: Standalone: RFG0010, RFG0011 $*/
/* CC_REF_NEW */
/* CC_REF_OLD */
--+=====================================================================+
--|                       R**** F***** G****                            |
--|                       P****** S****                                 |
--+=====================================================================+
--|                                                                     |
--| TYPE          : Package Body                                        |
--| NAME          : xxtest_har_pkg                                      |
--| PURPOSE       : Created for test purpose                            |
--|                                                                     |
--| Modification History:                                               |
--| ---------------------                                               |
--|                                                                     |
--| Author           Date        Version         Description            |
--| -----------    -----------  ------------   ----------------         |
--| (Harsha)        15-AUG-2019  11.5.10.1      Initial revision        |
--| (Harsha)        09-SEP-2019  11.5.10.2      RFG0002 - Updated       |
--| (Harsha)        09-SEP-2019  11.5.10.3      RFG0003 - Updated       |
--| (Harsha)        18-SEP-2019  11.5.10.4      RFG0005 - Updated       |
--| (Harsha)        18-SEP-2019  11.5.10.5      RFG0006 - Updated       |
--| (Harsha)        18-SEP-2019  11.5.10.6      RFG0007 - Updated       |
--| (Harsha)        18-SEP-2019  11.5.10.7      RFG0010 - Updated       |
--| (Arpita)        23-SEP-2019  11.5.10.8      RFG0011 - Updated 
--| (Arpita)        24-SEP-2019  11.5.10.9      RFG0012 - Updated       |
--| (Arpita)        24-SEP-2019  11.5.10.10     RFG0013 - Updated       |
--| (Arpita)        24-SEP-2019  11.5.10.11     RFG0014 - Updated       |
--| (Arpita)        24-SEP-2019  11.5.10.12     RFG0015 - Updated       |
--| (Arpita)        24-SEP-2019  11.5.10.13     RFG0016- Updated        |
 --+====================================================================+
--+=======================================================================+
--| TYPE             : Procedure                                          |
--| NAME             : xxtest_har_output                                  |
--| INPUT Parameters : p_opco                                             |
--| OUTPUT Parameters: x_errbuff                                          |
--|                   ,x_retcode                                          |
--| PURPOSE          : Created for test purpose                           |
--| Modification History:                                                 |
--|----------------------                                                 |
--|                                                                       |
--| Author            Date        Version         Description             |
--| -----------     -----------  ------------     ----------------        |
--| (Harsha)        15-AUG-2019  11.5.10.1      Initial revision          |
--| (Harsha)        09-SEP-2019  11.5.10.2      RFG0002 - Updated         |
--| (Harsha)        09-SEP-2019  11.5.10.3      RFG0003 - Updated         |
--| (Harsha)        18-SEP-2019  11.5.10.4      RFG0005 - Updated         |
--| (Harsha)        18-SEP-2019  11.5.10.5      RFG0006 - Updated         |
--| (Harsha)        18-SEP-2019  11.5.10.6      RFG0007 - Updated         |
--| (Harsha)        18-SEP-2019  11.5.10.7      RFG0010 - Updated         |
--| (Arpita)        23-SEP-2019  11.5.10.8      RFG0011 - Updated         |
--+=======================================================================+
  PROCEDURE xxtest_har_output(
             x_errbuff OUT NOCOPY VARCHAR2
            ,x_retcode OUT NOCOPY NUMBER
            ,p_opco hr_operating_units.organization_id%TYPE
            )
  IS 
  
    CURSOR l_opco_name_cur(
             cp_opco hr_operating_units.organization_id%TYPE
             )
    IS
      SELECT hou.name
        FROM hr_operating_units hou
       WHERE hou.organization_id = cp_opco;
  
    g_opco_name       hr_operating_units.name%TYPE;
   
BEGIN
  
  OPEN l_opco_name_cur(
         cp_opco => p_opco
        );
  FETCH l_opco_name_cur
  INTO g_opco_name;
  CLOSE l_opco_name_cur;
  -- Ver No: 11.5.10.2 is added for RFG0002
  -- Ver No: 11.5.10.3 is added for RFG0003
  -- Ver No: 11.5.10.4 is added for RFG0005
  -- Ver No: 11.5.10.5 is added for RFG0006
  -- Ver No: 11.5.10.6 is added for RFG0007
  -- Ver No: 11.5.10.6 is added for RFG0008
  -- Ver No: 11.5.10.6 is added for RFG0009
  -- Ver No: 11.5.10.7 is added for RFG0010
  -- Ver No: 11.5.10.8 is added for RFG0011
  fnd_file.put_line ( fnd_file.log, g_opco_name );  
  
END xxtest_har_output;
END xxtest_har_pkg;
/
